trigger UpdateFavouriteGenreTrigger on Lending__c (after insert, after update, after delete) {
    List<Lending__c> records;
    records = trigger.isDelete ? trigger.old : trigger.new;

    Set<id> userSet = new Set<id>();
    for (Lending__c l : records){
        userSet.add(l.Library_User__c);
    }
    
    List<AggregateResult> aggregates = [
        SELECT Library_User__c,Book__r.genre__c genre, count(id)
        FROM Lending__c 
        WHERE Library_User__c IN :userSet
        GROUP BY Library_User__c, Book__r.genre__c
        ORDER BY Library_User__c,count(id) DESC
        ];

    Id curId = null;
    List<Library_User__c> updatedUsers = new List<Library_User__c>();
    Library_User__c u;
    String s;
    Id userid;
    for (AggregateResult ar : aggregates)
    {
        userid = (Id)ar.get('Library_User__c');
        if (userid != curId || curId==null){
            u = new Library_User__c();
            u.Id = userid;
            s = (String)ar.get('genre');
            u.Favourite_Genre__c = s;
            updatedUsers.add(u);
            curId = userid;
        }
    }
    System.debug(updatedUsers.size());
    update updatedUsers;

}