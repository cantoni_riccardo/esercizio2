@isTest
public class UpdateFavouriteGenreTest {
    
    @isTest static void test(){
        Library__c l = new Library__c();
        l.name = 'testlib';
        l.Street__c = 'test street';
        insert l;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'][0]; 
        Library_User__c u = new Library_User__c();
        u.Name = 'test';
        u.Library__c = l.id;
        u.Gender__c = 'M';
        insert u;
        
        Book__c b = new Book__c();
        b.Name = 'testBook';
        b.Genre__c = 'Drama';
        b.Total_Copies__c = 50;
        b.Library__c = l.id;
        insert b;
        
        Lending__c lending = new Lending__c();
        Lending.Book__c = b.Id;
        Lending.Library_User__c = u.Id;
        Lending.Lending_Start__c = Date.today();
        insert lending;
        
        u = [SELECT Favourite_genre__c from Library_User__c WHERE id = :u.Id][0];
        System.assert(u.Favourite_Genre__c == 'Drama');
    }
    
    

}